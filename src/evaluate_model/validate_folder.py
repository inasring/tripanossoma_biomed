import os
import numpy as np
import imageio
from train import train
from dir_utils import dir_utils as du

def save_img_array(image_name_list,image_array):
    imageio.imwrite(os.path.join(true_positives_dir,str(image_index)+'.jpg'),save_array[image_index])


def validate_img_dir(directory,model,targets,output_directory='output_dir/',image_extension_list=["png","jpg"],threshold=0.5):
    dataset = train.load_images(directory=directory,image_extension_list=image_extension_list)
    preprocessed_image = train.preprocess(dataset.astype(np.float32))
    prediction = model.predict(preprocessed_image)
    prediction_threshold = prediction>threshold
    print("prediction:",prediction)
    print("prediction.shape:",prediction.shape)
    # print("targets:",targets)
    print("prediction_threshold.shape:",prediction_threshold.shape)
    print("targets.shape:",targets.shape)
    # exit()
    targets = targets.squeeze()
    prediction_threshold = prediction_threshold.squeeze()
    true_positives = np.where(np.logical_and(prediction_threshold,targets))
    # true_positives = np.logical_and(prediction_threshold.squeeze(),targets.squeeze())
    print("true_positives:",len(true_positives))
    # exit()
    true_negatives = np.where(np.logical_and(np.logical_not(prediction_threshold).squeeze(),np.logical_not(targets)))
    false_positives= np.where(np.logical_and(prediction_threshold,np.logical_not(targets).squeeze()))
    false_negatives= np.where(np.logical_and(np.logical_not(prediction_threshold).squeeze(),targets))
    du.make_dir(output_directory)
    true_positives_dir = os.path.join(output_directory,'true_positives/')
    true_negatives_dir = os.path.join(output_directory,'true_negatives/')
    false_positives_dir = os.path.join(output_directory,'false_positives/')
    false_negatives_dir = os.path.join(output_directory,'false_negatives/')
    
    du.make_dir(true_positives_dir)
    du.make_dir(true_negatives_dir)
    du.make_dir(false_positives_dir)
    du.make_dir(false_negatives_dir)
    print('dataset:',dataset.shape)
    print('true_positives:',true_positives)
    print('true_positives:',len(true_positives))

    dataset = dataset.astype(np.uint8)
    save_array = dataset[true_positives]
    print(save_array.shape)
    # exit()
    for image_index in range(len(save_array)):
        # print("image_index:",image_index)
        imageio.imwrite(os.path.join(true_positives_dir,str(image_index)+'.jpg'),save_array[image_index])
        # print("saving...",os.path.join(true_positives_dir,str(image_index)+'.jpg'))
    save_array = dataset[true_negatives]
    for image_index in range(len(save_array)):
        imageio.imwrite(os.path.join(true_negatives_dir,str(image_index)+'.jpg'),save_array[image_index])
    save_array = dataset[false_positives]
    for image_index in range(len(save_array)):
        imageio.imwrite(os.path.join(false_positives_dir,str(image_index)+'.jpg'),save_array[image_index])
    save_array = dataset[false_negatives]
    for image_index in range(len(save_array)):
        imageio.imwrite(os.path.join(false_negatives_dir,str(image_index)+'.jpg'),save_array[image_index])    


if __name__ == '__main__':
    from keras.models import load_model
    model_filename = '../model/model.hdf5'
    model = load_model(model_filename)
    validation_dir = "../dataset_tripanossoma_compatible_v3/val"
    validation_target_filename = "../dataset_tripanossoma_compatible_v3/target_val.txt"
    validation_targets = np.loadtxt(validation_target_filename)
    validate_img_dir(directory=validation_dir,
                    model=model,
                    targets=validation_targets,
                    output_directory='output_dir/',
                    image_extension_list=["png","jpg"],threshold=0.5)