from keras.models import load_model
import numpy as np

from train import train

validation_dir = "../augmented_dataset/val"
validation_target_filename = "../augmented_dataset/target_val.txt"
model_filename = "../model/model.hdf5"

model = load_model(model_filename)
print("Loading Images...")
validation_input =train.load_images(validation_dir)
validation_target = np.loadtxt(validation_target_filename)
print("Preprocessing Images...")
validation_input = train.preprocess(validation_input)
confiancas = model.predict(validation_input)

preds_corretas = (confiancas>0.5).astype(int).squeeze() == validation_target  # Predicoes corretas
indexes = np.where((confiancas>0.5).astype(int).squeeze() != validation_target) # indices das amostras erradas
print("predicoes corretas:",sum(preds_corretas))
print("numero de amostras:",len(validation_target))
print("taxa acerto:",sum(preds_corretas)/len(validation_target))
print("indexes errados:",indexes)