import os
import glob
import importlib

import numpy as np
import imageio

from dir_utils import dir_utils as du

def find_filenames(directory,extensions):
    filename_list = []
    for extension in extensions:
        string_match = os.path.join(directory+"/*."+extension)
        filename_list.extend(
            glob.glob(string_match)
            )
    return filename_list


def load_images(directory,image_extension_list=["png","jpg"]):
    filenames = find_filenames(directory,image_extension_list)
    #Assumes all images have the same shape
    if not filenames:
        error_msg = "No file with extensions "\
                    +str(image_extension_list)+" were found at "\
                    +directory
        raise FileNotFoundError(error_msg)
    sample_image = imageio.imread(filenames[0])
    shape = sample_image.shape
    nsamples = len(filenames)
    dataset = np.empty(tuple([nsamples])+shape)
    
    # for i,filename in enumerate(filenames):
    #     dataset[i] = imageio.imread(filename)
    for i,filename in enumerate(filenames):
        filename_no_extension = os.path.splitext(filename)[0]
        filename_no_path = os.path.basename(filename_no_extension)
        dataset[int(filename_no_path)] = imageio.imread(filename)
    
    return dataset

def preprocess(array):
    """Simple preprocessing to map the dataset
        from 0-255 to 0-1
    """
    array/=127.5
    array-=1.
    return array

def get_model(model_name,kwargs):
    keras_applications = importlib.import_module('keras.applications')
    model = getattr(keras_applications,model_name)
    return model(**kwargs)

def add_model_top(feature_extractor_model,top_layer_model,input_shape):
    from keras.models import Model
    from keras.layers import Input
    complete_model_input = Input(shape=input_shape)
    complete_model_features = feature_extractor_model(complete_model_input)
    complete_model_predictions = top_layer_model(complete_model_features)
    complete_model = Model(inputs=[complete_model_input],
                                outputs=[complete_model_predictions])
    return complete_model

def train(train_dir,
            validation_dir,
            train_target_filename,
            validation_target_filename,
            model_output_directory="../model",
            model_output_filename="model.hdf5",
            history_filename='history.txt',
            save_complete_model_cached=True,
            data_augmentation=False,
            train_features=False):

    import keras.applications as keras_apps
    from keras.preprocessing.image import ImageDataGenerator
    from keras.models import Model,load_model
    from keras.layers import Input,Dense,Dropout
    from keras.optimizers import Adam
    from keras.callbacks import ModelCheckpoint,TerminateOnNaN,ReduceLROnPlateau

    from keras.backend.tensorflow_backend import set_session
    import tensorflow as tf
    config = tf.ConfigProto()
    # config.gpu_options.per_process_gpu_memory_fraction = 0.6
    config.gpu_options.allow_growth=True
    set_session(tf.Session(config=config))

    train_input      =load_images(train_dir)
    validation_input =load_images(validation_dir)
    du.make_dir(model_output_directory)
    print("train_input.shape:",train_input.shape)
    single_image_shape = train_input.shape[1:]
    # batch_size = 15#32
    batch_size = 32
    # epochs     = 100
    epochs     = 10
    terminate_on_nan = TerminateOnNaN()
    model_output_filename_path = os.path.join(model_output_directory,
                                    model_output_filename)

    train_target = np.loadtxt(train_target_filename)
    validation_target = np.loadtxt(validation_target_filename)
    
    train_input = preprocess(train_input)
    train_input_shape_original = train_input.shape
    validation_input = preprocess(validation_input)
    
    feature_extractor_model= keras_apps.mobilenet_v2.MobileNetV2(input_shape=single_image_shape,
                                                            alpha=1.0,
                                                            include_top=False,
                                                            weights='imagenet',
                                                            input_tensor=None,
                                                            pooling="avg")
    print("feature_extractor_model.outputs.shape=",feature_extractor_model.output.shape[1])
    inp = Input(shape=tuple([int(feature_extractor_model.output.shape[1])])) #Terrible but works
    x=inp
    # x   = Dropout(0.95)(x)
    x   = Dropout(0.2)(x)
    x   = Dense(1,activation="sigmoid")(x)
    prediction = x
    top_layer_model = Model(inputs=[inp],outputs=[prediction])
    reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.3,
                              patience=8, min_lr=0.00001)



    if data_augmentation:
        print("Caching features is not available if data_augmentation is enabled\
                     as it requires constant input images")
        datagen = ImageDataGenerator(
                        rotation_range=20,
                        width_shift_range=0.2,
                        height_shift_range=0.2,
                        horizontal_flip=True,
                        vertical_flip=True)
        # datagen = ImageDataGenerator(
        #             rotation_range=0,
        #             width_shift_range=0,
        #             height_shift_range=0,
        #             horizontal_flip=False,
        #             vertical_flip=False)
        feature_extractor_model.trainable = train_features # Trainable = True is better
        for layer in feature_extractor_model.layers:
            layer.trainable = train_features
        datagen.fit(train_input)
        complete_model = add_model_top(feature_extractor_model,
                                        top_layer_model,
                                        train_input_shape_original[1:])

        complete_model.compile(Adam(),
                                loss="binary_crossentropy",
                                metrics=['accuracy'])

        model_checkpoint = ModelCheckpoint(model_output_filename_path,
                                            monitor='val_acc',
                                            verbose=1,
                                            save_best_only=True,
                                            save_weights_only=False,
                                            mode='auto')                                            
        callbacks = [model_checkpoint,terminate_on_nan,reduce_lr]
        complete_model.summary()
        history = complete_model.fit_generator(
                        datagen.flow(train_input,
                                    train_target,
                                    batch_size=batch_size,
                                    shuffle=True),
                        validation_data=(validation_input,validation_target),
                        steps_per_epoch = len(train_input)/batch_size,
                        epochs=epochs,
                        callbacks=callbacks)
        
        # Test without data augmentation
        # complete_model.fit(
        #         train_input,
        #         train_target,
        #         batch_size=batch_size,
        #         shuffle=True,
        #         validation_data=(validation_input,validation_target),
        #         epochs=epochs,
        #         callbacks=callbacks)

        checkpoint_model = load_model(model_output_filename_path)
        return checkpoint_model

    else:
        train_input = feature_extractor_model.predict(train_input)
        validation_input = feature_extractor_model.predict(validation_input)
        


        top_layer_model.compile(Adam(),
                                loss="binary_crossentropy",
                                metrics=['accuracy'])
        top_model_output_filename_path = os.path.join(model_output_directory,
                                            "top_model.hdf5")                                   

        top_model_checkpoint = ModelCheckpoint(top_model_output_filename_path,
                                            monitor='val_acc',
                                            verbose=1,
                                            save_best_only=True,
                                            save_weights_only=False,
                                            mode='auto')
        callbacks = [top_model_checkpoint,terminate_on_nan,reduce_lr]

        history = top_layer_model.fit(x=train_input,
                            y=train_target,
                            validation_data=(validation_input,validation_target),
                            batch_size=batch_size,
                            epochs=epochs,
                            callbacks=callbacks)

        
        print("Loading checkpoint model...")
        checkpoint_top_model = load_model(top_model_output_filename_path)
        print("train_input_shape_original:",train_input_shape_original[1:])
        checkpoint_model = add_model_top(feature_extractor_model,
                                        checkpoint_top_model,
                                        train_input_shape_original[1:])

        if save_complete_model_cached:
            print("Saving complete model...")
            checkpoint_model.save(model_output_filename_path)
    
    history_filename_path = os.path.join(model_output_directory,history_filename)
    headers = ['acc','val_acc','loss','val_loss']
    history_save = np.vstack([history.history[key] for key in headers]).T
    np.savetxt(history_filename_path,history_save)
    
    return checkpoint_model,history


if __name__ == "__main__":
    # x = load_images(directory="../dataset/train")
    # print(x.shape)
    # print(x[-1])
    # train_dir = "../dataset/train"
    # validation_dir = "../dataset/validation"
    # train_target_filename = "../dataset/train/train_targets.txt"
    # validation_target_filename = "../dataset/validation/validation_targets.txt"
    
    # train_dir = "../dataset_tripanossoma_compatible/train"
    # validation_dir = "../dataset_tripanossoma_compatible/val"
    # train_target_filename = "../dataset_tripanossoma_compatible/target_train.txt"
    # validation_target_filename = "../dataset_tripanossoma_compatible/target_val.txt"

    # train_dir = "../dataset_tripanossoma_compatible_v2/train"
    # validation_dir = "../dataset_tripanossoma_compatible_v2/val"
    # train_target_filename = "../dataset_tripanossoma_compatible_v2/target_train.txt"
    # validation_target_filename = "../dataset_tripanossoma_compatible_v2/target_val.txt"

    # train_dir = "../dataset_tripanossoma_compatible_v3/train"
    # validation_dir = "../dataset_tripanossoma_compatible_v3/val"
    # train_target_filename = "../dataset_tripanossoma_compatible_v3/target_train.txt"
    # validation_target_filename = "../dataset_tripanossoma_compatible_v3/target_val.txt"

    train_dir = "../augmented_dataset/train"
    validation_dir = "../augmented_dataset/val"
    train_target_filename = "../augmented_dataset/target_train.txt"
    validation_target_filename = "../augmented_dataset/target_val.txt"
    model_output_directory="../model"

    data_augmentation = False

    model,history = train(train_dir,
                        validation_dir,
                        train_target_filename,
                        validation_target_filename,
                        model_output_directory=model_output_directory,
                        save_complete_model_cached=True,
                        data_augmentation=data_augmentation,
                        train_features=False)
    
    validation_input =load_images(validation_dir)
    validation_target = np.loadtxt(validation_target_filename)
    validation_input = preprocess(validation_input)
    confiancas = model.predict(validation_input)

    preds_corretas = (confiancas>0.5).astype(int).squeeze() == validation_target  # Predicoes corretas
    indexes = np.where((confiancas>0.5).astype(int).squeeze() != validation_target) # indices das amostras erradas
    print("predicoes corretas:",sum(preds_corretas))
    print("numero de amostras:",len(validation_target))
    print("taxa acerto:",sum(preds_corretas)/len(validation_target))
    print("indexes errados:",indexes)


    
    from matplotlib import pyplot as plt

    fig = plt.figure()
    ax  = fig.add_subplot(111)
    n_epochs = len(history.history['loss'])
    epochs = range(n_epochs)
    ax.plot(epochs,history.history['loss'],label='loss')
    ax.plot(epochs,history.history['val_loss'],label='val_loss')
    ax.legend()

    fig2 = plt.figure()
    ax2  = fig2.add_subplot(111)
    ax2.plot(epochs,history.history['acc'],label='acc')
    ax2.plot(epochs,history.history['val_acc'],label='val_acc')
    ax2.legend()

    plt.show()