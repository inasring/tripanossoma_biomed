import os
from glob import glob
import imageio 

def get_name(name):
    nobase_name = os.path.basename(name)
    noextension_nobase_name = os.path.splitext(nobase_name)[0]
    return noextension_nobase_name

def get_name_list(name_list):
    ret = []
    for name in name_list:
        ret.append(get_name(name))
    return ret

def add_extension_list(filename_list,extension):
    ret = []
    for name in filename_list:
        ret.append(name+extension)
    return ret

def convert_image_list(input_directory,input_extension,save_directory,save_extension):
    glob_image_get = os.path.join(input_directory,"*"+input_extension)
    image_files = glob(glob_image_get)
    print(glob_image_get)
    get_name_list(image_files)
    name_list_ext = get_name_list(image_files)
    names_with_extension = add_extension_list(name_list_ext,save_extension)
    names_with_extension_with_dir = [os.path.join(save_directory,names) for names in names_with_extension]
    for i,original_image_name in enumerate(image_files):
        loaded_file = imageio.imread(original_image_name)
        imageio.imwrite(names_with_extension_with_dir[i],loaded_file)
        print("Saving ",names_with_extension_with_dir[i]+" ...")
    print (names_with_extension_with_dir)



input_image_dir = "Fotos_da_lamina_removida/"
input_image_extension = ".jpg"

output_image_dir = "tripanossoma_converted_format/"
output_image_extension = ".tiff"

# glob_image_get = os.path.join(input_image_dir,"*"+input_image_extension)
# image_files = glob(glob_image_get)
# print(image_files)
# name_list_ext = get_name_list(image_files)
# print(name_list_ext)
# print(add_extension_list(name_list_ext,output_image_extension))
convert_image_list(input_image_dir,input_image_extension,output_image_dir,output_image_extension)