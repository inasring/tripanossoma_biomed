import os
import numpy as np
import imageio
from keras.preprocessing.image import ImageDataGenerator
from keras.preprocessing.image import array_to_img
from train import train
from dir_utils import dir_utils as du

def augment_dataset(dataset_dir,
                    targets_filename,
                    image_output_dir,
                    target_output_filename,
                    multiply_dataset_size = 1,
                    batch_size = 150,
                    extension=".jpg"):
    du.make_dir(image_output_dir)
    image_input = train.load_images(train_dir)
    target = np.loadtxt(targets_filename)
    n_samples_augmented = multiply_dataset_size*len(image_input)
    augmented_target = np.empty(n_samples_augmented)
    n_steps_per_epoch = len(image_input)//batch_size+int((len(image_input)%batch_size)!=0)
    print(n_steps_per_epoch)

    datagen = ImageDataGenerator(
                    rotation_range=0,
                    width_shift_range=0.1,
                    height_shift_range=0.1,
                    horizontal_flip=True,
                    vertical_flip=True)
    # This iterator generates exacly the same number of outputs as inputs in the first epoch
    datagen_iterator = datagen.flow(image_input,
                                    target,
                                    batch_size=batch_size,
                                    shuffle=False,
                                    save_to_dir=None,
                                    save_prefix='')

    outside_index = 0
    print("total_epochs:",multiply_dataset_size)
    for epoch in range(multiply_dataset_size):
        print("epoch:",epoch)
        for step in range(n_steps_per_epoch):
            images,targets  = next(datagen_iterator)
            for image_index in range(len(images)):
                im_name = str(outside_index)+extension
                im_name_full = os.path.join(image_output_dir,im_name)
                imageio.imwrite(im_name_full,np.uint8(images[image_index]))
                augmented_target[outside_index] = targets[image_index]
                outside_index+=1
            # print(images.shape)
    np.savetxt(target_output_filename,augmented_target)
        

if __name__ == "__main__":
    # train_dir = "../dataset_tripanossoma_compatible_v2/train"
    # train_target_filename = "../dataset_tripanossoma_compatible_v2/target_train.txt"
    # augmented_dataset_dir_train = "../augmented_dataset/train"
    # augmented_dataset_dir_train_target = "../augmented_dataset/target_train.txt"

    train_dir = "../dataset_tripanossoma_compatible_v3/train"
    train_target_filename = "../dataset_tripanossoma_compatible_v3/target_train.txt"
    augmented_dataset_dir_train = "../augmented_dataset_v3/train"
    augmented_dataset_dir_train_target = "../augmented_dataset_v3/target_train.txt"

    augment_dataset(train_dir,
                    train_target_filename,
                    augmented_dataset_dir_train,
                    augmented_dataset_dir_train_target,
                    multiply_dataset_size=25)

