import os

import imageio
import numpy as np
from PIL import Image

from dir_utils import dir_utils as du

def save_image_array(array,directory,leading_zeros=6):
    for i in range(len(array)):
        filename = str(i).zfill(leading_zeros)+".png"
        filename_path = os.path.join(directory,filename)
        imageio.imwrite(filename_path,array[i].astype(np.uint8))

def resize_image_array(array,target_shape=(224,224,3),mode="RGB"):
    return_array_shape = tuple([len(array)])+target_shape
    target_image_row_col = target_shape[:-1]
    ret_array = np.empty(return_array_shape)
    
    for i in range(len(array)):
        ret_array[i] = np.array(
                        Image.fromarray(array[i]).resize(target_image_row_col)
                        )
    return ret_array

def make_cifar10_folders(nsamples=200,test_size=0.33,
                        random_state=42,output_directory="../dataset",
                        train_targets_filename = "train_targets.txt",
                        validation_targets_filename = "validation_targets.txt",
                        select_y=None,
                        resize=False):
    from keras.datasets import cifar10
    from sklearn.model_selection import train_test_split
    (x_train, y_train), (_, _) = cifar10.load_data()

    if select_y is not None:
            selected_indexes = np.where(select_y(y_train))
            print("selected_indexes:",selected_indexes)
            y_train = y_train[selected_indexes]
            x_train = x_train[selected_indexes[0],:,:,:]
    print("y_train.shape:",y_train.shape)
    print("x_train.shape:",x_train.shape)
    
    x_train, x_val, y_train, y_val = train_test_split(x_train, y_train,
                                        random_state=random_state,
                                        test_size=test_size)
    x_train = x_train[:nsamples]
    y_train = y_train[:nsamples]
    x_val = x_val[:nsamples]
    y_val = y_val[:nsamples]
    
    if resize:
        x_train = resize_image_array(x_train,target_shape=(224,224,3),mode="RGB")
        x_val   = resize_image_array(x_val  ,target_shape=(224,224,3),mode="RGB")

    train_output_directory = os.path.join(output_directory,"train/")
    validation_output_directory = os.path.join(output_directory,"validation/")
    
    du.make_dir(output_directory)
    du.make_dir(train_output_directory)
    du.make_dir(validation_output_directory)

    train_targets_filename_path = os.path.join(train_output_directory,
                                                train_targets_filename)
    validation_targets_filename_path = os.path.join(validation_output_directory,
                                        validation_targets_filename)                                                
    print("Saving training samples...")
    np.savetxt(train_targets_filename_path,y_train)
    save_image_array(x_train,train_output_directory)
    print("Saving validation samples...")
    np.savetxt(validation_targets_filename_path,y_val)
    save_image_array(x_val,validation_output_directory)

if __name__ == "__main__":
    def zero_or_one(array):
        """Returns an array with True at the positions 
        where the array is equal to one or zero and False otherwise"""
        return np.logical_or(array == 1,array == 0)

    make_cifar10_folders(nsamples=1000,test_size=0.33,
            random_state=42,output_directory="../dataset",
            train_targets_filename = "train_targets.txt",
            validation_targets_filename = "validation_targets.txt",
            select_y=zero_or_one,
            resize=True)
