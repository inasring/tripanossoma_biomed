import glob
import os
import shutil
import numpy as np
from PIL import Image

def make_dir(directory):
    try:
        os.makedirs(directory)
    except FileExistsError:
        pass

def get_class(string_list):
    filename = [os.path.split(fname)[-1] for fname in string_list]
    img_type = [string.split(".")[0][-1] for string in filename]
    return img_type

def to_01(l):
    return map(lambda x: 0 if x=="n" else 1,l)

def to_class(l):
    return list(to_01(get_class(l)))

def rename_files(file_list,target_dir,extension=".jpg"):
    # base_names = map(os.path.basename,filelist)
    new_names = [os.path.join(target_dir,str(i))+extension for i in range(len(file_list))]
    # print(new_names)
    for i,name in enumerate(file_list):
        shutil.copy(name,new_names[i])
    return new_names

def resize_file_inplace(filename,width,height):
    im = Image.open(filename)
    im = im.resize((width,height))
    im.save(filename)
    print("Saving:",filename)

directory = "../dataset_tripanossoma_v3/dataset_tripanossoma_v3/"
train_dir = os.path.join(directory,"train/")
val_dir = os.path.join(directory,"val/")

target_directory = "../dataset_tripanossoma_compatible_v3"
train_target_dir = os.path.join(target_directory,"train/")
val_target_dir   = os.path.join(target_directory,"val/")

train_target_filename = "target_train.txt"
val_target_filename = "target_val.txt"

extension = ".jpg"

make_dir(train_target_dir)
make_dir(val_target_dir)

files_train= glob.glob(train_dir+"/*"+extension)
files_val= glob.glob(val_dir+"/*"+extension)

filename_train_targets = os.path.join(target_directory,train_target_filename)
filename_val_targets = os.path.join(target_directory,val_target_filename)

train_classes = to_class(files_train)
val_classes = to_class(files_val)

np.savetxt(filename_train_targets,train_classes)
np.savetxt(filename_val_targets,val_classes)


files_train_newdir = rename_files(files_train,train_target_dir)
files_val_newdir = rename_files(files_val,val_target_dir)

width = 224
height = 224

list(map(lambda filename:resize_file_inplace(filename,width,height),files_train_newdir))
list(map(lambda filename:resize_file_inplace(filename,width,height),files_val_newdir))


